import os
import random
import json
import time
import requests
from bs4 import BeautifulSoup


FILE_DIR = os.path.dirname(os.path.abspath(__file__))


class ApartmentDotComListing(object):
    """
    Functions containing utitlies to extract apartment listing from 
    apartments.com

    Parameters
    ------------
    """
    def __init__(self):
        self.headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0'}



    def get_page_listings(self, url, index=-1):
        """
        Parameters
        ----------
        url : str
            url of the listing page

        index : int
            index of the page
        """

        r = requests.get(url, headers=self.headers)

        response = dict()
        if r.status_code == 200:
            soup = BeautifulSoup(r.text, 'html.parser')
            tag = soup.find(id='placardContainer')
            if not tag:
                print("This is not a listing page.\
                 Please check. URL : {}".format(url))
                return response
            else:
                url_list = list()
                for node in tag.find_all('li'):
                    # print(node)
                    try:
                        apt_url = node.find('article')['data-url']
                    except (TypeError, KeyError) as e:
                        continue
                    url_list.append(apt_url)
                response["urlList"] = url_list

            
            # check for number of pages
            if index == 0:
                tag = soup.find(id='paging')
                if not tag:
                    return response
                else:
                    pages = list()
                    for node in tag.find_all('a'):
                        try:
                            pg = int(node['data-page'])
                        except TypeError as e:
                            # print(node)
                            continue
                        pages.append(pg)
                response["maxpage"] = max(pages)
                

        return response



if __name__ == "__main__":
    adcl = ApartmentDotComListing()

    apartment_urls = list()

    # base_url = "https://www.apartments.com/boston-ma/"

    # read json file
    file_path = os.path.join(FILE_DIR, "..", "data", "listing_pages.json")

    with open(file_path) as f:
        js = json.load(f)
    cities = {"natick"}
    for listing in js[1:]:
        base_url = listing["URL"]

        city = listing["city"]
        if city not in cities:
            continue



        results = adcl.get_page_listings(base_url, index=0)
        apartment_urls.extend(results["urlList"])

        for i in range(2, results['maxpage']+1):
            url = base_url + str(i) + '/'
            
            delay = random.randint(1, 10)
            print("Delay of : {}".format(delay))
            time.sleep(delay)

            print("WORKING ON URL : {}".format(url))
            results = adcl.get_page_listings(url)
            apartment_urls.extend(results.get("urlList"))

        with open(base_url.strip().split('/')[-2] + ".txt", "a") as f:
            for url1 in apartment_urls:
                f.write(url1+ "\n")



