import os
import sys
from datetime import datetime
import time
import random
import requests
from pymongo import MongoClient
from data_collection.templates import ApartmentsDotCom

FILE_DIR = os.path.dirname(os.path.abspath(__file__))

client = MongoClient('mongodb://localhost:27017')


def main(env='dev', row=0):
    """
    Parameters
    -----------
    env : str
        enviroment to use, i.e., dev/prod
    """

    if env == 'dev':
        db = client["apt-rates-dev"]
    elif env == 'prod':
        db = client["apt-rates-prod"]
    else:
        raise ValueError("env can either be 'dev' or\
         'prod' you had `{}`".format(env))


    print("CURRENTLY RUNNING IN {} ENVIRONMENT".format(env))


    collection = db["aptData"]
        
    f = open(os.path.join(FILE_DIR, "urls", "boston_urls.txt"))
    headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0'}

    i = 0
    for l in f:
        url = l.strip()
        i += 1
        if i < row:
            continue
        apt_name = url.split('/')[3]
        print("{}. WORKING ON URL : {}".format(i, url))
        r = requests.get(url, headers=headers)

        delay = random.randint(1, 30)
        print("Waiting for {} seconds .....".format(delay))
        time.sleep(delay)
        adc = ApartmentsDotCom(html=r.text)
        try:
            all_data = adc.extract_apartment_listing(apt_name=apt_name)
        except (AttributeError, IndexError) as e:
            print("==" * 20)
            print("ATTRIBUTE ERROR : {}".format(e))
            print("==" * 20)
            print("\n\n")
            continue
            
        
        # print(all_data)
        collection.insert_many(all_data.copy())
        # for data in all_data:
        #     print(data)
        #     collection.insert_one(data.copy())



if __name__ == "__main__":
    args = sys.argv
    
    if len(args) == 2:
        main(args[1])
    elif len(args) == 3:
        main(args[1], int(args[2]))
    else:
        main()


    