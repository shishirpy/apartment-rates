from datetime import datetime
import pprint
import re
from bs4 import BeautifulSoup

class ApartmentsDotCom():
    """
    Class to handle pages from http://apartments.com

    Parameters
    -------------
    html : str
        html of the page from which data needs to be extracted
    """

    def __init__(self, html=None):
        if html:
            self.soup = BeautifulSoup(html, 'html.parser')
            self.html = html

    def type_page(self, text):
        """
        Determine the type of page
        """
        # TODO: this is yet to be implemented
        pass


    def extract_apartment_listing(self, apt_name, html=None):
        """
        Extract list from apartment page.

        Parameters
        ---------------
        html : str (defaul None)
            html of the page form which data needs to be extracted
        """

        if html != None:
            self.soup = BeautifulSoup(html, 'html.parse')
            self.html = html

        tag = self.soup.find('table', {'class':re.compile('^availabilityTable$')})
        tag1 = self.soup.find('table', {'class': 'availabilityTable oneRental'})
        if not tag1:
            tag2 = self.soup.find('table', {'class': 'availabilityTable basic oneRental'})
            if tag2:
                tag1 = tag2

        all_data = list()
        if tag1:
            # print(123)
            node = tag.find('tr', {'class': 'rentalGridRow'})
            # print(node)
            data_dict = dict()
            data_dict["apartmentName"] = apt_name
            data_dict['baths'] = node.get('data-baths')
            data_dict['beds'] = node.get('data-beds')
            data_dict["maxRent"] = node.get('data-maxrent')
            data_dict["model"] = node.get('data-model')
            data_dict['rentalKey'] = node.get('data-rentalkey')
            data_dict['rentalType'] = node.get('data-rentaltype')
            data_dict['unit'] = node.get('data-unit')

            rent_node = node.find('td', {'class': 'rent'})
            # print(rent_node)
            rents = rent_node.text.strip().split('-')
            if len(rents) > 1:
                data_dict['minRent'] = int(rents[0][1:].strip().replace(',', ''))
            else:
                data_dict['minRent'] = data_dict["maxRent"]

            area_node = node.find('td', {'class':'sqft'})
            if (area_node) and (area_node.text.strip() != ""):
                data_dict["area"] = int(area_node.text.strip().split()[0].replace(',', ''))
            else:
                data_dict["area"] = ""

            available_node = node.find('td', {'class': 'available'})
            if available_node:
                data_dict['available'] = available_node.text.strip()

            data_dict['timeStamp'] = datetime.timestamp(datetime.now())

            all_data.append(data_dict)
            # pprint.pprint(all_data)
            return all_data

            

        for idx, node in enumerate(tag.findAll('tr')):
            data_dict = dict()
            data_dict["apartmentName"] = apt_name
            data_dict["baths"] = node.get('data-baths')
            data_dict["beds"] = node.get('data-beds')
            data_dict["maxRent"] = node.get('data-maxrent')
            data_dict["model"] = node.get('data-model')
            data_dict['rentalKey'] = node.get('data-rentalkey')
            data_dict['rentalType'] = node.get('data-rentaltype')
            data_dict['unit'] = node.get('data-unit')
            
            rent_node = node.find('td', {'class': 'rent'})
            rents = rent_node.text.strip().split('-')
            if len(rents) > 1:
                data_dict['minRent'] = int(rents[0][1:].strip().replace(',', ''))
            else:
                data_dict['minRent'] = data_dict["maxRent"]


            area_node = node.find('td', {'class':'sqft'})
            if (area_node) and (area_node.text.strip() != ""):
                data_dict["area"] = int(area_node.text.strip().split()[0].replace(',', ''))
            else:
                data_dict["area"] = ""
            
            # print("area 123")
            available_node = node.find('td', {'class': 'available'})
            if available_node:
                data_dict['available'] = available_node.text.strip()

            data_dict['timeStamp'] = datetime.timestamp(datetime.now())
            
            all_data.append(data_dict)
        # pprint.pprint(all_data)  
        return all_data



if __name__ == "__main__":
    import requests
    # from fake_user import UserAgent
    
    headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0'}

    url = "https://www.apartments.com/one-india-street-boston-ma/71bcmyz/"
    url = "https://www.apartments.com/audubon-plantation-ridge-worcester-ma/syl2f65/"
    url = "https://www.apartments.com/royal-worcester-apartments-worcester-ma/ge3sxqw/"
    url = "https://www.apartments.com/the-tremont-boston-ma/l495qmg/"
    url = "https://www.apartments.com/the-eugene-new-york-ny/thftp68/"
    # url = "https://www.apartments.com/the-tremont-boston-ma/l495qmg/"
    url = "https://www.apartments.com/123-kittredge-st-boston-ma/h4y0t22/"

    r = requests.get(url, headers=headers)

    adc = ApartmentsDotCom(r.text)
    adc.extract_apartment_listing(apt_name="123")