import os

FILE_DIR = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
    'sqlite:///' + os.path.join(FILE_DIR, "book_keeping.db")

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    